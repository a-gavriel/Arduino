
#include <IRremote.h>
#include "LowPower.h"

IRsend irsend;

const int b4  = 4;  //bluetooth
const int b5  = 5;  //mute
const int b6  = 6;  //vol up
const int b7  = 7;  //vol down
const int b8  = 8;  //bass up
const int b9  = 9;  //bass down
const int b10  = 10; //treble up
const int b11  = 11; //treble down
const int b12  = 12; //Changes the mode to TV


int timer;
int modeCounter = 0;



void wakeUp() {
  timer = 0;
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(b4, INPUT);
  pinMode(b5, INPUT);
  pinMode(b6, INPUT);
  pinMode(b7, INPUT);
  pinMode(b8, INPUT);
  pinMode(b9, INPUT);
  pinMode(b10, INPUT);
  pinMode(b11, INPUT);
  pinMode(b12, INPUT);
}

void loop() {
  attachInterrupt(0, wakeUp, HIGH);
  while (timer < 10000) {
    if (digitalRead(b4) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xAF5E817, 32);
    }

    if (digitalRead(b5) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xD5AD0B51, 32);
    }

    if (digitalRead(b6) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xDE5526F7, 32);
    }

    if (digitalRead(b7) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0x8C549057, 32);
    }

    if (digitalRead(b8) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xD5AD0B51, 32);
    }

    if (digitalRead(b9) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xFD58A7 , 32);
    }

    if (digitalRead(b10) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xFDA25D, 32);
    }

    if (digitalRead(b11) == HIGH) {
      timer = 0;
      delay(50);
      irsend.sendNEC(0xFD629D, 32);
    }

    if (digitalRead(b12) == HIGH) {
      //can add another button
    }
    delay(1);
    timer = timer + 1;

  }
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}
