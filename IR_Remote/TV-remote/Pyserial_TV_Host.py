# Importing Libraries
import serial
import pynput
from pynput.keyboard import Key
from pynput.mouse import Button
import sys, time
import serial.tools.list_ports




ARROW_RIGHT = "E0E046B9"
ARROW_LEFT  = "E0E0A659"
ARROW_UP    = "E0E006F9"
ARROW_DOWN  = "E0E08679"
ENTER_KEY   = "E0E016E9"
BUTTON_RED  = "E0E036C9"
BUTTON_GREEN= "E0E028D7"



def get_COM_PORT() -> str:
  ports = list(serial.tools.list_ports.comports())
  for p in ports:
    if "CH340" in p.description:
      com_port = p.device
      return com_port
  return ""



def initialize_arduino():
  com_port = get_COM_PORT()
  try:
    arduino = serial.Serial(port=(com_port), baudrate=9600, timeout=.1)
    return arduino
  except:
    print("Could not open COM port")
    for remaining in range(10, 0, -1):
      sys.stdout.write("\r")
      sys.stdout.write("{:2d} seconds remaining.".format(remaining)) 
      sys.stdout.flush()
      time.sleep(1)
    exit()


def main():
  arduino = initialize_arduino()
  print("Initialized TV Controller")
  keyboard = pynput.keyboard.Controller()
  mouse = pynput.mouse.Controller()

  value : str = ""
  value_bytes : bytes = 0
  while True:
      value_bytes = arduino.readline()
      value = value_bytes.decode("utf-8")
      value = value.replace("\n","").replace("\r","")
      if value != "":
        print(value) 
        if value == BUTTON_RED:   # Single click
          mouse.click(Button.left, 1)

        elif value == BUTTON_RED:   # Double click
          mouse.click(Button.left, 1)
          time.sleep(0.1)
          mouse.click(Button.left, 1)

        elif value == ENTER_KEY:    # PAUSE
          keyboard.press(Key.media_play_pause)
          keyboard.release(Key.media_play_pause)

        elif value == ARROW_RIGHT:    # right
          keyboard.press(Key.right)
          keyboard.release(Key.right)

        elif value == ARROW_LEFT:    # left
          keyboard.press(Key.left)
          keyboard.release(Key.left)

        elif value == ARROW_UP:    # up
          keyboard.press(Key.media_volume_up)
          keyboard.release(Key.media_volume_up)

        elif value == ARROW_DOWN:    # down
          keyboard.press(Key.media_volume_down)
          keyboard.release(Key.media_volume_down)

if __name__ == "__main__":
  main()