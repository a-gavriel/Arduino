/*
 * IRremote: IRsendDemo - demonstrates sending IR codes with IRsend
 * An IR LED must be connected to Arduino PWM pin 3.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 */


#include <IRremote.h>

IRsend irsend;

const int b4  = 4;
const int b5  = 5;
const int b6  = 6;
const int b7  = 7;

void setup()
{
  pinMode(b4, INPUT);
  pinMode(b5, INPUT);
  pinMode(b6, INPUT);
  pinMode(b7, INPUT);
}

void loop() {
  if (digitalRead(b4) == HIGH) {
  	irsend.sendPanasonic(0x4004, 0x538BC81);
    delay(40);
  }

  
  if (digitalRead(b5) == HIGH) {
    irsend.sendRC6(0xDF24CDE2, 28);
    delay(40);
  }

  
  if (digitalRead(b6) == HIGH) {
    irsend.sendPanasonic(0x4004, 0x5008481);
    delay(40);
  }

  
  if (digitalRead(b7) == HIGH) {
    irsend.sendPanasonic(0x4004, 0x5000401);
    delay(40);
  }
  
  
}
