
#include <TimerOne.h>

//buttons and leds pins
const int LeftB_pin = A4;
const int RightB_pin = A5;
const int ResetB_pin = A2;
const int RedLed_pin = A0;
const int GreenLed_pin = A1;


//the pins of 4-digit 7-segment display attach to pin2-13 respectively 
const int a = 2;
const int b = 3;
const int c = 4;
const int d = 5;
const int e = 6;
const int f = 7;
const int g = 8;
const int p = 9;

const int d4 = 10;
const int d3 = A3;
const int d2 = 12;
const int d1 = 13;



int LeftB = 0;
int RightB = 0;
int ResetB = 0;

int down = 0;
int accepted = 0;
int finished = 0;
int counter = 0;

long n = 0;// n represents the value displayed on the LED display. For example, when n=0, 0000 is displayed. The maximum value is 9999. 
int x = 100;
int del = 5;//Set del as 5; the value is the degree of fine tuning for the clock

void setup() {
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(p, OUTPUT);


  pinMode(LeftB_pin, INPUT);
  pinMode(RightB_pin, INPUT);
  pinMode(ResetB_pin, INPUT);
  pinMode(RedLed_pin, OUTPUT);
  pinMode(GreenLed_pin, OUTPUT);


  Timer1.initialize(100000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
  Timer1.attachInterrupt( add ); // attach the service routine here


}

void loop() {

  
  LeftB = digitalRead(LeftB_pin);
  RightB = digitalRead(RightB_pin);
  ResetB = digitalRead(ResetB_pin);
  down = LeftB & RightB;

  
  
  finished = 0;
  n = 0;// n represents the value displayed on the LED display. For example, when n=0, 0000 is displayed. The maximum value is 9999. 
  x = 100;
  counter = 0;


  

  // Waits until RightB and LeftB both have a value of 1 for 0.55 seconds

  while(counter < 55){
    
    LeftB = digitalRead(LeftB_pin);
    RightB = digitalRead(RightB_pin);
    down = LeftB & RightB;
    delay(10);
    if (down){
      ++counter;  
      digitalWrite(RedLed_pin, HIGH);
    }else{
      counter = 0;
      digitalWrite(RedLed_pin, LOW);
    }

  }
  digitalWrite(RedLed_pin, LOW);
  digitalWrite(GreenLed_pin, HIGH);

  // Waits until release of at least one button

  while(down){
    LeftB = digitalRead(LeftB_pin);
    RightB = digitalRead(RightB_pin);
    down = LeftB & RightB;
    delay(10);
  }

  
  Timer1.restart();
  
  digitalWrite(GreenLed_pin, LOW);   

  // Displays time and counts while both buttons are NOT pressed

  while(!down){
    LeftB = digitalRead(LeftB_pin);
    RightB = digitalRead(RightB_pin);
    down = LeftB & RightB;
    
    clearLEDs();//clear the 7-segment display screen
    clearLEDs();//clear the 7-segment display screen
    pickDigit(0);//Light up 7-segment display d1
    pickNumber((n/1000));// get the value of thousand
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(1);//Light up 7-segment display d2
    pickNumber((n%1000)/100);// get the value of hundred
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(2);//Light up 7-segment display d3
    pickNumber(n%100/10);//get the value of ten
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(3);//Light up 7-segment display d4
    pickNumber(n%10);//Get the value of single digit
    delay(del);//delay 5ms

  }
  Timer1.stop();
  finished = 1;
  ResetB = digitalRead(ResetB_pin);

  // Keeps the time and waits for the reset Button

  while(!ResetB & finished){
    clearLEDs();//clear the 7-segment display screen
    clearLEDs();//clear the 7-segment display screen
    pickDigit(0);//Light up 7-segment display d1
    pickNumber((n/1000));// get the value of thousand
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(1);//Light up 7-segment display d2
    pickNumber((n%1000)/100);// get the value of hundred
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(2);//Light up 7-segment display d3
    pickNumber(n%100/10);//get the value of ten
    delay(del);//delay 5ms
  
    clearLEDs();//clear the 7-segment display screen
    pickDigit(3);//Light up 7-segment display d4
    pickNumber(n%10);//Get the value of single digit
    delay(del);//delay 5ms

    ResetB = digitalRead(ResetB_pin);
    
  }


  
}

/**************************************/ 
void pickDigit(int x) //light up a 7-segment display
{
  //The 7-segment LED display is a common-cathode one. So also use digitalWrite to  set d1 as high and the LED will go out
  digitalWrite(d1, LOW);
  digitalWrite(d2, LOW);
  digitalWrite(d3, LOW);
  digitalWrite(d4, LOW);

  switch(x)
  {
    case 0: 
    digitalWrite(d1, HIGH);//Light d1 up 
    break;
    case 1: 
    digitalWrite(d2, HIGH); //Light d2 up 
    break;
    case 2: 
    digitalWrite(d3, HIGH); //Light d3 up 
    digitalWrite(p, LOW);
    break;
    default: 
    digitalWrite(d4, HIGH); //Light d4 up     
    break;
  }
}


//The function is to control the 7-segment LED display to display numbers. Here x is the number to be displayed. It is an integer from 0 to 9 
void pickNumber(int x)
{
  switch(x)
  {
    default: 
    zero(); 
    break;
    case 1: 
    one(); 
    break;
    case 2: 
    two(); 
    break;
    case 3: 
    three(); 
    break;
    case 4: 
    four(); 
    break;
    case 5: 
    five(); 
    break;
    case 6: 
    six(); 
    break;
    case 7: 
    seven(); 
    break;
    case 8: 
    eight(); 
    break;
    case 9: 
    nine(); 
    break;
  }
}


void clearLEDs() //clear the 7-segment display screen
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(p, HIGH);
}

void zero() //the 7-segment led display 0
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void one() //the 7-segment led display 1
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void two() //the 7-segment led display 2
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void three() //the 7-segment led display 3
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void four() //the 7-segment led display 4
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void five() //the 7-segment led display 5
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void six() //the 7-segment led display 6
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void seven() //the 7-segment led display 7
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void eight() //the 7-segment led display 8
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void nine() //the 7-segment led display 9
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}


/*******************************************/
void add()
{
  // Toggle LED  
  n++;
  if(n == 10000){
    n = 0;
  }
  
}
