const int DIGITAL_LED_PIN  = 3;
const int SENSOR_PC_ON_PIN = A7;
const int DIGITAL_CONTROL_PIN = 6;
bool time_expired = false;
bool measuring_time = false;
bool control_signal_on = false;
bool pc_on = false;
int analog_pc_on = 0;

#define INPUT_RESOLUTION 1024 / 5   // Resolution is 10bits / 5 Volts max  = 0.0049
#define CUTOFF_VOLTAGE_V 0.5  // Value in volts
#define CUTOFF_VOLTAGE_VALUE INPUT_RESOLUTION * CUTOFF_VOLTAGE_V  // Value in read units
#define WAIT_TIME_BEFORE_RECHECK_ON_MS 1000

const unsigned long TURN_ON_START_MS = 100;
const unsigned long TURN_ON_END_MS = 3000;
const unsigned long TURN_OFF_START_MS = 8000;
const unsigned long TURN_OFF_END_MS = 12000;

unsigned long control_on_time_ms = 0;
unsigned long control_off_time_ms = 0;

unsigned int control_duration;


void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(DIGITAL_CONTROL_PIN, INPUT_PULLUP);
  pinMode(DIGITAL_LED_PIN, OUTPUT);
  time_expired = false;
  measuring_time = false;
  control_signal_on = false;
  pc_on = false;
  control_duration = 0;
  analog_pc_on = 0;
}

void turn_on_pc(){
  const int POWER_BUTTON_HOLD_DURATION_MIN_MS = 500;
  const int POWER_BUTTON_HOLD_DURATION_SCALE_MS = 250;
  const int TURN_ON_TRIES = 5;

  for (int i = 0; i < TURN_ON_TRIES; i++ ){
    if (!is_pc_on()){
      digitalWrite(DIGITAL_LED_PIN, HIGH);                  // push button
      delay(POWER_BUTTON_HOLD_DURATION_MIN_MS + (i * POWER_BUTTON_HOLD_DURATION_SCALE_MS));        // wait for half second
      digitalWrite(DIGITAL_LED_PIN, LOW);    // turn the LED off by making the voltage LOW
      delay(POWER_BUTTON_HOLD_DURATION_MIN_MS);      // wait for half second
    }
    delay(WAIT_TIME_BEFORE_RECHECK_ON_MS);      // wait for 1 second
  }
  return;
}

void turn_off_pc(){
  const int TURN_OFF_PUSH_BUTTON_MS = 10000;
  
  digitalWrite(DIGITAL_LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(TURN_OFF_PUSH_BUTTON_MS);      // wait for a second
  digitalWrite(DIGITAL_LED_PIN, LOW);    // turn the LED off by making the voltage LOW
  return;
}

int analog_input_value(int analog_pin){
  const int measurements = 4;
  int sensor_value;
  int avg_value = 0;
  
  for (int i = 0; i<measurements; i++){
    sensor_value = analogRead(analog_pin);
    sensor_value = sensor_value / measurements;
    avg_value = avg_value + sensor_value;
    delay(10);  
  }

  return avg_value;
}



bool is_pc_on(){
  analog_pc_on = analog_input_value(SENSOR_PC_ON_PIN);
  return analog_pc_on > CUTOFF_VOLTAGE_V;
}


void loop() {
  control_signal_on = !digitalRead(DIGITAL_CONTROL_PIN);  // Negate due to it being pulled-up
  pc_on = is_pc_on();

  if (!control_signal_on){
    measuring_time = false;
  }

  if ((!measuring_time) && control_signal_on){
    measuring_time = true;
    time_expired = false;
    control_on_time_ms = millis();
  }

  if (measuring_time){
    control_duration = millis() - control_on_time_ms;  
    if (control_duration > TURN_OFF_END_MS){
      time_expired = true;
    }  
  }

  if ((!measuring_time) && (!time_expired)){
    
    if ((control_duration > TURN_ON_START_MS) && (control_duration < TURN_ON_END_MS)){
      if (!pc_on){
          turn_on_pc();
          time_expired = true;
      }
    }
  
    if ((control_duration > TURN_OFF_START_MS) && (control_duration < TURN_OFF_END_MS)){
      if (pc_on){
          turn_off_pc();
          time_expired = true;
      }
    }  

    control_duration = 0;
  }
  
  Serial.print("control_signal_on = ");
  Serial.print(control_signal_on);
  Serial.print("\t analog_pc_on = ");
  Serial.print(analog_pc_on);
  Serial.print("\t pc_on = ");
  Serial.print(pc_on);
  Serial.print("\t measuring_time = ");
  Serial.print(measuring_time);
  Serial.print("\t time_expired = ");
  Serial.print(time_expired);
  Serial.print("\t control_duration = ");
  Serial.print(control_duration);
  Serial.print("\t measuring_time = ");
  Serial.println(measuring_time);


}
