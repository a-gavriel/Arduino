/*
   Leonardo WiiClassicController GamePad by Nguyen Chi Tam, created in March 2017

   This sketch uses some codes from
   https://github.com/TarkanAl-Kazily/Wii_Classic_Controller/blob/master/Wii_Classic_Controller.ino

   and ArduinoJoystickLibrary from Matthew Heironimus
   https://github.com/MHeironimus/ArduinoJoystickLibrary
*/

#include <Joystick.h>
#include <Wire.h>

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID, JOYSTICK_TYPE_GAMEPAD,
                   16, 1,                  // Button Count, Hat Switch Count
                   true, true, false,     // X and Y, but no Z Axis
                   true, true, false,     // Rx and Ry, but no Rz Axis
                   false, false,          // No rudder or throttle
                   false, false, false);  // No accelerator, brake, or steering

#define CLASSIC_BYTE_COUNT (6)

/*    Bit
  Byte  7 6 5 4 3 2 1 0
  0     RX<4:3>  |                  LX<5:0>
  1 RX<2:1>      |                  LY<5:0>
  2 RX<0> |   LT<4:3>   |           RY<4:0>
  3 LT<2:0>             |           RT<4:0>
  4 BDR BDD BLT B-  BH  B+  BRT Null
  5 BZL BB  BY  BA  BX  BZR BDL BDU
*/

struct ClassicController {
  int8_t LeftX;
  int8_t LeftY;
  int8_t RightX;
  int8_t RightY;
  int8_t LeftT;
  int8_t RightT;
  boolean ButtonDown;
  boolean ButtonLeft;
  boolean ButtonUp;
  boolean ButtonRight;
  boolean ButtonSelect;
  boolean ButtonHome;
  boolean ButtonStart;
  boolean ButtonY;
  boolean ButtonX;
  boolean ButtonA;
  boolean ButtonB;
  boolean ButtonL;
  boolean ButtonR;
  boolean ButtonZL;
  boolean ButtonZR;
};

/**
   @brief Write bytes out to a device over I2C

   @param buf is a pointer to a string of bytes
   @param len is the length of the string
*/
void wire_write(uint8_t *buf, size_t len) {
  if ((buf != NULL) && (len > 0)) {
    Wire.beginTransmission(*buf++);
    while (--len != 0) {
      Wire.write(*buf++);
    }
    Wire.endTransmission();
  }
}

uint8_t init_string_1[] = {0x52, 0xf0, 0x55}; // Comment about what this string does
uint8_t init_string_2[] = {0x52, 0xfb, 0x00}; // Comment about what this string does
uint8_t zero_string[] = {0x52, 0x00};

void send_zero() {
  wire_write(zero_string, sizeof(zero_string));
}

void controller_init() {
  wire_write(init_string_1, sizeof(init_string_1));
  wire_write(init_string_2, sizeof(init_string_2));
}

void controller_decode_bytes(uint8_t *buf, size_t len, struct ClassicController *myStruct) {

  if ((buf == NULL) || (len < CLASSIC_BYTE_COUNT) || (myStruct == NULL)) {
    return;
  }

  myStruct->LeftX = (buf[0] & (64 - 1)) - 32; // 0 to 63
  myStruct->LeftY = (buf[1] & (64 - 1)) - 32; // 0 to 63 -> -32 to 31

  myStruct->RightX = (((buf[2] >> 7) & 1) + ((buf[1] >> 6) & 3) * 2 + ((buf[0] >> 6) & 3) * 8) - 16; // 0 to 31 -> -16 to 15
  myStruct->RightY = (buf[2] & (32 - 1)) - 16; // 0 to 31 -> -16 to 15

  //  Serial.print(myStruct->RightX);
  //  Serial.print('\n');
  //  Serial.print(myStruct->RightY);

  myStruct->LeftT = (((buf[2] >> 5) & 3) * 8 + ((buf[3] >> 5) & 7));
  myStruct->RightT = (buf[3] & (32 - 1));

  myStruct->ButtonDown = ((buf[4] & (1 << 6)) == 0);
  myStruct->ButtonLeft = ((buf[5] & (1 << 1)) == 0);
  myStruct->ButtonUp = ((buf[5] & (1 << 0)) == 0);
  myStruct->ButtonRight = ((buf[4] & (1 << 7)) == 0);
  myStruct->ButtonSelect = ((buf[4] & (1 << 4)) == 0);
  myStruct->ButtonHome = ((buf[4] & (1 << 3)) == 0);
  myStruct->ButtonStart = ((buf[4] & (1 << 2)) == 0);
  myStruct->ButtonY = ((buf[5] & (1 << 5)) == 0);
  myStruct->ButtonX = ((buf[5] & (1 << 3)) == 0);
  myStruct->ButtonA = ((buf[5] & (1 << 4)) == 0);
  myStruct->ButtonB = ((buf[5] & (1 << 6)) == 0);
  myStruct->ButtonL = ((buf[4] & (1 << 5)) == 0);
  myStruct->ButtonR = ((buf[4] & (1 << 1)) == 0);
  myStruct->ButtonZL = ((buf[5] & (1 << 7)) == 0);
  myStruct->ButtonZR = ((buf[5] & (1 << 2)) == 0);
}

int16_t PovMap[3][3] = {
  {315, 0, 45},
  {270, -1, 90},
  {225, 180, 135}
};

#define BUTTON_TYPE_XBOX 0
#define BUTTON_TYPE_STANDARD 1
int buttonType = BUTTON_TYPE_XBOX;

void setup() {
  pinMode(13, OUTPUT); // for testing purpose

  Joystick.begin();
  Joystick.setXAxisRange(-32, 31);
  Joystick.setYAxisRange(-32, 31);
  Joystick.setRxAxisRange(-16, 15);
  Joystick.setRyAxisRange(-16, 15);

  Wire.begin ();
  controller_init();
}

void loop() {
  // put your main code here, to run repeatedly:
  struct ClassicController Wii;
  uint8_t rawbytes[CLASSIC_BYTE_COUNT];    // array to store arduino output
  size_t cnt = 0;

  send_zero(); // send the request for next bytes
  delay(20);
  Wire.requestFrom(0x52, CLASSIC_BYTE_COUNT);  // request data from nunchuck
  while (Wire.available()) {
    rawbytes[cnt++] = Wire.read();
  }

  // If we recieved the 6 bytes, then do something with them
  if (cnt >= CLASSIC_BYTE_COUNT) {
    controller_decode_bytes(rawbytes, cnt, &Wii);

    if (Wii.ButtonHome) {
      if (buttonType == BUTTON_TYPE_XBOX) {
        buttonType = BUTTON_TYPE_STANDARD;
        digitalWrite(13, HIGH);
      } else {
        buttonType = BUTTON_TYPE_XBOX;
        digitalWrite(13, LOW);
      }
      delay(1000);
    } else {
      if (buttonType == BUTTON_TYPE_XBOX) {
        if (Wii.ButtonL) {
          Joystick.pressButton(6);
        } else {
          Joystick.releaseButton(6);
        }

        if (Wii.ButtonR) {
          Joystick.pressButton(7);
        } else {
          Joystick.releaseButton(7);
        }

        if (Wii.ButtonZL) {
          Joystick.pressButton(4);
        } else {
          Joystick.releaseButton(4);
        }

        if (Wii.ButtonZR) {
          Joystick.pressButton(5);
        } else {
          Joystick.releaseButton(5);
        }

        if (Wii.ButtonSelect) {
          Joystick.pressButton(9);
        } else {
          Joystick.releaseButton(9);
        }

        if (Wii.ButtonStart) {
          Joystick.pressButton(8);
        } else {
          Joystick.releaseButton(8);
        }

        if (Wii.ButtonX) {
          Joystick.pressButton(3);
        } else {
          Joystick.releaseButton(3);
        }

        if (Wii.ButtonY) {
          Joystick.pressButton(0);
        } else {
          Joystick.releaseButton(0);
        }

        if (Wii.ButtonA) {
          Joystick.pressButton(2);
        } else {
          Joystick.releaseButton(2);
        }

        if (Wii.ButtonB) {
          Joystick.pressButton(1);
        } else {
          Joystick.releaseButton(1);
        }

        int xDim = (Wii.ButtonLeft ? 0 : (Wii.ButtonRight ? 2 : 1));
        int yDim = (Wii.ButtonUp ? 0 : (Wii.ButtonDown ? 2 : 1));
        Joystick.setHatSwitch(0, PovMap[yDim][xDim]);

      } else {
        if (Wii.ButtonL) {
          Joystick.pressButton(4);
        } else {
          Joystick.releaseButton(4);
        }

        if (Wii.ButtonR) {
          Joystick.pressButton(5);
        } else {
          Joystick.releaseButton(5);
        }

        if (Wii.ButtonZL) {
          Joystick.pressButton(6);
        } else {
          Joystick.releaseButton(6);
        }

        if (Wii.ButtonZR) {
          Joystick.pressButton(7);
        } else {
          Joystick.releaseButton(7);
        }

        if (Wii.ButtonSelect) {
          Joystick.pressButton(8);
        } else {
          Joystick.releaseButton(8);
        }

        if (Wii.ButtonStart) {
          Joystick.pressButton(9);
        } else {
          Joystick.releaseButton(9);
        }

        if (Wii.ButtonX) {
          Joystick.pressButton(3);
        } else {
          Joystick.releaseButton(3);
        }

        if (Wii.ButtonY) {
          Joystick.pressButton(2);
        } else {
          Joystick.releaseButton(2);
        }

        if (Wii.ButtonA) {
          Joystick.pressButton(1);
        } else {
          Joystick.releaseButton(1);
        }

        if (Wii.ButtonB) {
          Joystick.pressButton(0);
        } else {
          Joystick.releaseButton(0);
        }

        if (Wii.ButtonLeft) {
          Joystick.pressButton(14);
        } else {
          Joystick.releaseButton(14);
        }

        if (Wii.ButtonRight) {
          Joystick.pressButton(15);
        } else {
          Joystick.releaseButton(15);
        }

        if (Wii.ButtonUp) {
          Joystick.pressButton(12);
        } else {
          Joystick.releaseButton(12);
        }

        if (Wii.ButtonDown) {
          Joystick.pressButton(13);
        } else {
          Joystick.releaseButton(13);
        }
      }

      Joystick.setXAxis(Wii.LeftX);
      Joystick.setYAxis(-Wii.LeftY);
      Joystick.setRxAxis(Wii.RightX);
      Joystick.setRyAxis(-Wii.RightY);

//      Joystick.sendState();
      delay(50);
    }
  }
  delay(1);
}
