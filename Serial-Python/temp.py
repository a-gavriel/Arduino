import serial
from time import sleep
import sys
###########################################################
import ctypes
from ctypes import wintypes
import time

user32 = ctypes.WinDLL('user32', use_last_error=True)

INPUT_MOUSE    = 0
INPUT_KEYBOARD = 1
INPUT_HARDWARE = 2

KEYEVENTF_EXTENDEDKEY = 0x0001
KEYEVENTF_KEYUP       = 0x0002
KEYEVENTF_UNICODE     = 0x0004
KEYEVENTF_SCANCODE    = 0x0008

MAPVK_VK_TO_VSC = 0

# msdn.microsoft.com/en-us/library/dd375731
VK_TAB  = 0x09
VK_MENU = 0x12

# C struct definitions

wintypes.ULONG_PTR = wintypes.WPARAM

class MOUSEINPUT(ctypes.Structure):
    _fields_ = (("dx",          wintypes.LONG),
                ("dy",          wintypes.LONG),
                ("mouseData",   wintypes.DWORD),
                ("dwFlags",     wintypes.DWORD),
                ("time",        wintypes.DWORD),
                ("dwExtraInfo", wintypes.ULONG_PTR))

class KEYBDINPUT(ctypes.Structure):
    _fields_ = (("wVk",         wintypes.WORD),
                ("wScan",       wintypes.WORD),
                ("dwFlags",     wintypes.DWORD),
                ("time",        wintypes.DWORD),
                ("dwExtraInfo", wintypes.ULONG_PTR))

    def __init__(self, *args, **kwds):
        super(KEYBDINPUT, self).__init__(*args, **kwds)
        # some programs use the scan code even if KEYEVENTF_SCANCODE
        # isn't set in dwFflags, so attempt to map the correct code.
        if not self.dwFlags & KEYEVENTF_UNICODE:
            self.wScan = user32.MapVirtualKeyExW(self.wVk,
                                                 MAPVK_VK_TO_VSC, 0)

class HARDWAREINPUT(ctypes.Structure):
    _fields_ = (("uMsg",    wintypes.DWORD),
                ("wParamL", wintypes.WORD),
                ("wParamH", wintypes.WORD))

class INPUT(ctypes.Structure):
    class _INPUT(ctypes.Union):
        _fields_ = (("ki", KEYBDINPUT),
                    ("mi", MOUSEINPUT),
                    ("hi", HARDWAREINPUT))
    _anonymous_ = ("_input",)
    _fields_ = (("type",   wintypes.DWORD),
                ("_input", _INPUT))

LPINPUT = ctypes.POINTER(INPUT)

def _check_count(result, func, args):
    if result == 0:
        raise ctypes.WinError(ctypes.get_last_error())
    return args

user32.SendInput.errcheck = _check_count
user32.SendInput.argtypes = (wintypes.UINT, # nInputs
                             LPINPUT,       # pInputs
                             ctypes.c_int)  # cbSize

# Functions

def PressKey(hexKeyCode):
    x = INPUT(type=INPUT_KEYBOARD,
              ki=KEYBDINPUT(wVk=hexKeyCode))
    user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

def ReleaseKey(hexKeyCode):
    x = INPUT(type=INPUT_KEYBOARD,
              ki=KEYBDINPUT(wVk=hexKeyCode,
                            dwFlags=KEYEVENTF_KEYUP))
    user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

###########################################################
COM = 'COM3'# /dev/ttyACM0 (Linux)
BAUD = 115200

ser = serial.Serial(COM, BAUD, timeout = .1)

print('Waiting for device');
sleep(3)
print(ser.name)

#check args
if("-m" in sys.argv or "--monitor" in sys.argv):
	monitor = True
else:
	monitor= False
###########################################################



class key:
    num0 = 0x30
    num1 = 0x31
    num2 = 0x32
    num3 = 0x33
    num4 = 0x34
    num5 = 0x35
    num6 = 0x36
    num7 = 0x37
    num8 = 0x38
    num9 = 0x39
    a = 0x41
    b = 0x42
    c = 0x43
    d = 0x44
    e = 0x45
    f = 0x46
    g = 0x47
    h = 0x48
    i = 0x49
    j = 0x4A
    k = 0x4B
    l = 0x4C
    m = 0x4D
    n = 0x4E
    o = 0x4F
    p = 0x50
    q = 0x51
    r = 0x52
    s = 0x53
    t = 0x54
    u = 0x55
    v = 0x56
    w = 0x57
    x = 0x58
    y = 0x59
    z = 0x5A
    F1 = 0x70
    F2 = 0x71
    F3 = 0x72
    F4 = 0x73
    F5 = 0x74
    F6 = 0x75
    F7 = 0x76
    F8 = 0x77
    F9 = 0x78
    F10 = 0x79
    F11 = 0x7A
    F12 = 0x7B
    F13 = 0x7C
    F14 = 0x7D
    F15 = 0x7E
    F16 = 0x7F
    F17 = 0x80
    F18 = 0x81
    F19 = 0x82
    F20 = 0x83
    F21 = 0x84
    F22 = 0x85
    F23 = 0x86
    F24 = 0x87





# must be lower than 15 or update key_codes
numkeys = 9

keys = [False]*numkeys


# key_codes: letters range from: 0x41 - 0x5A
#0x4F = letter:  O
# B C D E F G H 
# 
#key_codes = [0x41,0x42,0x43,0x56,0x4E,0x28,0x26,0x4D,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F]

key_codes = [key.F8,key.F9,key.F10,key.F11,key.F12,key.F13,key.F14,key.F15,key.F16,key.F17,key.F18,key.F19,key.F20,key.F21,key.F22]

indexes = [0]*numkeys

key_names = ["Green", "Red", "Yellow", "Blue", "Orange", "Strum_Up", "Strum_Down", "Star", "Start", "Select"]

def switch_key(i,value):
	if value == '1':
		PressKey(key_codes[i])
	else:
		ReleaseKey(key_codes[i])


def setvalues(l):
	for i in range(numkeys):
		value = l[i]		
		if (value != "1" and value != "0"):
			value = "0"
			l[i] = value
		keys[i] = value



def getDiff(l):	
	for i in range(numkeys): 
		if l[i] != keys[i]:
			return i
	return -1

def setKeys(l):
	print("Don't press any Key... ",end="\r",flush = True)
	time.sleep(1)
	#Sets default Values
	setvalues(l)

	for i in range(numkeys):
		setting = True
		while(setting):
			val = str(ser.readline().decode('utf-8', errors='ignore').strip('\r\n'))#Capture serial output as a decoded string
			valA = val.split("/")	
			k = getDiff(valA)
			print("Diffkey =  " + str(k),end=" -",flush = True) 			
			if k != -1:
				indexes[i] = k
				setting = False
			print("Press the "+ key_names[i] +" Key: ",end=" --- ",flush = True)
			print(val, end="\r", flush=True)
			time.sleep(1)


def send_key(i):
	PressKey(key_codes[i])
	ReleaseKey(key_codes[i])

def check(l):
	value = ""
	for i in range(numkeys):
		value = l[i]
		if value != keys[i]:
			send_key(indexes[i])



while True:
	val = str(ser.readline().decode('utf-8', errors='ignore').strip('\r\n'))#Capture serial output as a decoded string
	valA = val.split("/")	
	setKeys(valA)
	time.sleep(1)
	check(valA)	
	#print(keys,end=" :: ",flush = True)
	if(monitor == True):
		#print(valA,end=" --- ",flush = True)
		print(val, end="\r", flush=True)



 #  