
/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial

/* Fill in information from Blynk Device Info here */
#define BLYNK_TEMPLATE_ID   "TMPL2uVA1pvJ5"
#define BLYNK_TEMPLATE_NAME "PC Switch"
#define BLYNK_AUTH_TOKEN    "TfWqWjFA-2MYcYh3jdeGe-z08Q5rZNXk"


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// WiFi credentials.
char ssid[] = "Joey doesn't share wifi!";
char pass[] = "yamevoy!";

// Blynk configuration
BlynkTimer timer;

// HW Config
const int SENSOR_PC_ON_PIN = A0;
const int DIGITAL_LED_PIN  = D4;
const int DIGITAL_CONTROL_PIN = D6;
int turn_on_signal_v1 = 0;
int turn_off_signal_v2 = 0;

// SW vars
const int ALIVE_TIMER_MAX = 10;
int alive_timer = ALIVE_TIMER_MAX;
int alive_pin_out = 0;

char measuring_step = 0;
char moving_average_size = 4;
int moving_average[]  = {0,0,0,0};


const int PC_ON_TIMER_START = 8;
const int PC_OFF_TIMER_START = 100;
int pc_turning_on_timer = 0;
int pc_turning_off_timer = 0;
bool executing_command = false;
bool pc_on_status = false;
bool output_signal = false;

#define DEBUG_VERSION 1


int get_pc_status(){
  #define INPUT_RESOLUTION 1024 / 3.3   // Resolution is 10bits / 3.3 Volts max  = 0.0049 u/V
  #define CUTOFF_VOLTAGE_V 0.5  // Value in volts
  #define CUTOFF_VOLTAGE_VALUE INPUT_RESOLUTION * CUTOFF_VOLTAGE_V  // Value in units
  const int CUTOFF_VOLTAGE = CUTOFF_VOLTAGE_VALUE;
  int average_measurement = 0;
  
  for (int i = 0; i < moving_average_size; i++){
    average_measurement += (moving_average[i] / moving_average_size);
  }

  return average_measurement > CUTOFF_VOLTAGE;
}


void read_analog_input(){
  char current_value = 0;
  
  current_value = analogRead(SENSOR_PC_ON_PIN);
  moving_average[measuring_step] = current_value;

  measuring_step = measuring_step + 1;
  if (measuring_step >= moving_average_size){
    measuring_step = measuring_step - moving_average_size;  
  }
}


// Blynk reading virtual pins
BLYNK_WRITE(V1)
{
  turn_on_signal_v1 = param.asInt();        // assigning incoming value from pin V1 to a variable
}

BLYNK_WRITE(V2)
{
  turn_off_signal_v2 = param.asInt();         // assigning incoming value from pin V1 to a variable
}



void turn_pc_on(){

  if (pc_turning_on_timer == PC_ON_TIMER_START){
    output_signal = true;
  }


  pc_turning_on_timer--;
  if (pc_turning_on_timer <= 0){
    output_signal = false;
  }
}



void write_output_pins(){
  digitalWrite(DIGITAL_LED_PIN, 1 - output_signal);  // LED is negated in HW
  digitalWrite(DIGITAL_CONTROL_PIN, output_signal);             
}


void turn_pc_off(){
  
  if (pc_turning_off_timer == PC_OFF_TIMER_START){
    output_signal = true;
  }


  pc_turning_off_timer--;
  if (pc_turning_off_timer <= 0){
    output_signal = false;
  }
}



// This function gets called every 10ms
//sends alternating bool every second to Virtual Pin (3).
void myTimerEvent(){
  read_analog_input();
  pc_on_status = get_pc_status();
  
  if (!executing_command && (turn_on_signal_v1 > 0) && (turn_off_signal_v2 == 0)){
    if (DEBUG_VERSION || !pc_on_status){
      pc_turning_on_timer = PC_ON_TIMER_START;
      executing_command = true;
    }
  }
  
  if (!executing_command && (turn_on_signal_v1 == 0) && (turn_off_signal_v2 > 0)){
    if (DEBUG_VERSION || pc_on_status){
      pc_turning_off_timer = PC_OFF_TIMER_START;
      executing_command = true;
    }
  }

  if (pc_turning_on_timer > 0){
    turn_pc_on();    
  }
  if (pc_turning_off_timer > 0){
    turn_pc_off();  
  }

  if (executing_command && (pc_turning_on_timer == 0) && (pc_turning_off_timer == 0) && 
        (turn_on_signal_v1 == 0) && (turn_off_signal_v2 == 0) ){
    executing_command = false;
  }


  alive_timer--;
  if (alive_timer <= 0){
    alive_timer = ALIVE_TIMER_MAX;
    alive_pin_out = 1 - alive_pin_out;
  }



  write_output_pins();
  Blynk.virtualWrite(V3, pc_on_status);
  Blynk.virtualWrite(V4, alive_pin_out);
  
  
}




void setup()
{
  

  Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass);
  timer.setInterval(10L, myTimerEvent);
  // Debug console
  Serial.begin(19200);
  
  pinMode(DIGITAL_LED_PIN, OUTPUT);
  pinMode(DIGITAL_CONTROL_PIN, OUTPUT);
  pinMode(SENSOR_PC_ON_PIN, INPUT);
}

void loop()
{
  Blynk.run();
  timer.run(); // Initiates BlynkTimer

  Serial.print("V1 = ");
  Serial.print(turn_on_signal_v1);
  Serial.print("\t V2 = ");
  Serial.print(turn_off_signal_v2);
  Serial.print("\t V3 = ");
  Serial.print(pc_on_status);
  Serial.print("\t DBG = ");
  Serial.print(DEBUG_VERSION);
  Serial.print("\t on_timer = ");
  Serial.print(pc_turning_on_timer);
  Serial.print("\t off_timer = ");
  Serial.print(pc_turning_off_timer);
  Serial.print("\t executing= ");
  Serial.print(executing_command);
  Serial.print("\t output_signal = ");
  Serial.println(output_signal);

  
}
